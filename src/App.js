import './App.css';
import { Routes, Route } from 'react-router-dom';

import RequireAuth from './components/auth/RequireAuth';
import RequireTeam from './components/teams/RequireTeam';
import Login from './components/auth/Login';
import Logout from './components/auth/Logout';
import Register from './components/auth/Register';
import Main from './components/Main';
import Talents from './components/pages/Talents';
import Events from './components/pages/Events';
import TalentProfile from './components/talents/TalentProfile';
import TalentsRoot from './components/talents/TalentsRoot';

import NewTeam from './components/teams/NewTeam';

import Tmp from './components/pages/Tmp';

const App = () => {
  return (
    <Routes>
      <Route index element={<Tmp />} />
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />

      <Route element={<RequireAuth />}>

        <Route element={<RequireTeam />}>

          <Route path="app/*" element={<Main />}>

            <Route path='talents/*' element={<Talents />}>
              <Route path=":id" element={<TalentProfile />} />
              <Route path="new" element={<TalentProfile newTalent={true} />} />
              <Route path="*" element={<TalentsRoot />} />
            </Route>

            <Route path='calendar' element={<Events />} />

          </Route>

        </Route>

        <Route path='app/teams/create' element={<NewTeam />} />
        <Route path='app/logout' element={<Logout />} />

      </Route>

      <Route path="*" element={<>404</>} />
    </Routes>
  );
}

export default App;
