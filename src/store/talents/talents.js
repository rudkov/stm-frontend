import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    shouldBeRefreshed: true, //TODO: delete after events refactored with thunks
    items: [],
    immutableItems: [],
    order: {
        'column': 'name',
        'asc': true
    },
    query: {
        'searchString': '',
        'inTownOnly': false,
    },
    selectedItems: [],
};

export const fetchTalents = createAsyncThunk('talents/fetchTalents', async () => {
    try {
        const response = await axios({
            method: 'get',
            url: '/api/v1/talents',
        });
        return response.data;
    } catch (err) {
        return err.message;
    }
});

const prepareTalentsCollection = (values) => {
    let items = values;

    values.map((item, index) => {
        item.full_name = [item.first_name, item.last_name].join(' ');

        item.phones = item.phones.sort(
            (a, b) => {
                if (a.type && b.type) {
                    return a.type.weight - b.type.weight
                }
                else
                    return -999999; //this hack is for sorting values without type (null values) at the bottom of the list
            });

        item.emails = item.emails.sort(
            (a, b) => {
                if (a.type && b.type) {
                    return a.type.weight - b.type.weight
                }
                else
                    return -999999; //this hack is for sorting values without type (null values) at the bottom of the list
            });

        return item;
    });

    return items;
};

const sortTalents = (items, order) => {
    if (order.column === 'name') {
        if (order.asc) {
            items.sort((a, b) => a.name?.localeCompare(b.name));
        }
        else {
            items.sort((a, b) => b.name?.localeCompare(a.name));
        }

    }
    else if (order.column === 'location') {
        items.sort((a, b) => {
            if (a.location === null) {
                return order.asc ? -1 : 1;
            }

            if (b.location === null) {
                return order.asc ? 1 : -1;
            }

            if (a.location === b.location) {
                return 0;
            }

            return a.location < b.location ? order.asc ? -1 : 1 : order.asc ? 1 : -1;
        });
    }
    else if (order.column === 'email') {
        items.sort((a, b) => {
            if (!a?.email || a.email === null) {
                return order.asc ? 1 : -1;
            }

            if (!b?.email || b.email === null) {
                return order.asc ? -1 : 1;
            }

            if (a.email === b.email) {
                return 0;
            }

            return a.email < b.email ? order.asc ? -1 : 1 : order.asc ? 1 : -1;
        });
    }
    else if (order.column === 'phone') {
        items.sort((a, b) => {
            if (!a?.phone || a.phone === null) {
                return order.asc ? 1 : -1;
            }

            if (!b?.phone || b.phone === null) {
                return order.asc ? -1 : 1;
            }

            if (a.phone === b.phone) {
                return 0;
            }

            return a.phone < b.phone ? order.asc ? -1 : 1 : order.asc ? 1 : -1;
        });
    }

    return items;
};

const filterTalents = (items, query) => {
    const searchString = query.searchString.toLowerCase();

    if (query.inTownOnly) {
        items = items.filter((item) => {
            return !item.location;
        });
    }

    items = items.filter((item) => {
        let r = false;

        if (item.name?.toLowerCase().includes(searchString)) {
            r = true;
        }
        else if (item.location?.toLowerCase().includes(searchString)) {
            r = true;
        }
        else if (item.email?.toLowerCase().includes(searchString)) {
            r = true;
        }
        else if (item.phone?.toLowerCase().includes(searchString)) {
            r = true;
        }
        return r;
    });

    return items;
};

const talentsSlice = createSlice({
    name: 'talents',
    initialState: initialState,
    reducers: {
        filter(state, action) {
            const items = state.immutableItems;
            state.query = action.payload.query;
            state.items = sortTalents(filterTalents(items, state.query), state.order);
        },
        sort(state, action) {
            const items = state.items;
            state.order = action.payload.order;
            state.items = sortTalents(filterTalents(items, state.query), state.order);
        },
        updateListFromServer(state, action) {  //TODO: delete after events refactored with thunks
            state.items = action.payload.items;
            state.immutableItems = action.payload.items;
            state.shouldBeRefreshed = false;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchTalents.fulfilled, (state, action) => {
                // state.immutableItems = prepareTalentsCollection(action.payload);
                // state.items = sortTalents(filterTalents(state.immutableItems, state.query), state.order);

                state.immutableItems = action.payload;
                state.items = sortTalents(filterTalents(state.immutableItems, state.query), state.order);
            })
    }
});

export const getTalents = (state) => state.talents.items;
export const getQuery = (state) => state.talents.query;
export const getOrder = (state) => state.talents.order;

export const talentsActions = talentsSlice.actions;

export default talentsSlice.reducer;