import { configureStore } from "@reduxjs/toolkit";
import settingsReducer from "./settings";
import authReducer from "./auth";
import talentReducer from './talents/talent';
import talentsReducer from "./talents/talents";
import eventReducer from "./events/event";
import eventsReducer from "./events/events";

const store = configureStore({
    reducer: {
        settings: settingsReducer,
        auth: authReducer,
        talent: talentReducer,
        talents: talentsReducer,
        event: eventReducer,
        events: eventsReducer,
    },
});

export default store;