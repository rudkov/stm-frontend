import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    shouldBeRefreshed: true,
    items: [],
};

const settingsSlice = createSlice({
    name: 'settings',
    initialState: initialState,
    reducers: {
        load(state) {
            state.shouldBeRefreshed = true;
        },
        init(state, action) {
            state.items = action.payload.items;
            state.shouldBeRefreshed = false;
        },
    },
});

export const settingsActions = settingsSlice.actions;

export default settingsSlice.reducer;