import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    shouldBeRefreshed: true,
    dates: {},
    talents: [],
    items: [],
};

const eventsSlice = createSlice({
    name: 'events',
    initialState: initialState,
    reducers: {
        updateList(state) {
            state.shouldBeRefreshed = true;
        },
        setDates(state, action) {
            state.dates = action.payload.items;
        },
        updateListFromServer(state, action) {
            state.items = action.payload.items;
            state.shouldBeRefreshed = false;
        },
        addOrRemoveTalent(state, action) {
            if (!state.talents.includes(action.payload.item.id)) {
                state.talents.push(action.payload.item.id);
            }
            else {
                const index = state.talents.indexOf(action.payload.item.id);
                if (index > -1) {
                    state.talents.splice(index, 1);
                }
            }
        },
    },
});

export const eventsActions = eventsSlice.actions;

export default eventsSlice.reducer;