import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    shouldBeRefreshed: false,
    selectedItem: {},
    item: {},
};

const eventSlice = createSlice({
    name: 'event',
    initialState: initialState,
    reducers: {
        setSelectedItem(state, action) {
            state.selectedItem = action.payload.item;
        },
        refresh(state) {
            state.shouldBeRefreshed = true;
        },
        update(state, action) {
            state.item = action.payload.item;
            state.shouldBeRefreshed = false;
        },
    },
});

export const eventActions = eventSlice.actions;

export default eventSlice.reducer;