import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import Settings from './settings';
import App from './App';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import axios from 'axios';
import store from './store/index';
import NotificationProvider from './components/notifications/NotificationProvider';

axios.defaults.baseURL = process.env.REACT_APP_URL;
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;

axios.get('/sanctum/csrf-cookie').then(response => {
  // Axios makes some magic and sets CSRF protection
});

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <BrowserRouter>
    <Provider store={store}>
      <NotificationProvider>
        <Settings />
        <Routes>
          <Route path="/*" element={<App />} />
        </Routes>
      </NotificationProvider>
    </Provider>
  </BrowserRouter>
);
