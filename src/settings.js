import axios from 'axios';

import { useDispatch, useSelector } from 'react-redux';
import { settingsActions } from './store/settings';

function Settings() {
  const dispatch = useDispatch();
  let shouldBeRefreshed = useSelector((state) => state.settings.shouldBeRefreshed);

  if (shouldBeRefreshed === true) {
    axios({
      method: 'get',
      url: '/api/v1/settings',
    })
      .then(function (response) {
        let items = response.data;
        dispatch(settingsActions.init({ items }));
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  return null;
}

export default Settings;