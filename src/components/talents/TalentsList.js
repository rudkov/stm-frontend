import './TalentsList.css';

import axios from 'axios';

import { Input, Form } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import TalentListItem from './TalentListItem';

import { talentsActions } from '../../store/talents/talents';

function TalentsList(props) {
  const dispatch = useDispatch();
  let shouldBeRefreshed = useSelector((state) => state.talents.shouldBeRefreshed) || [];
  let talents = useSelector((state) => state.talents.items) || [];
  let selectedTalents = useSelector((state) => state.events.talents) || [];

  const selectable = props.selectable || false;

  if (shouldBeRefreshed === true) {
    axios({
      method: 'get',
      url: '/api/v1/talents',
    })
      .then(function (response) {
        let items = response.data;
        dispatch(talentsActions.updateListFromServer({ items }));
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const mainAction = (item) => {
    props.mainAction(item);
  }

  const secondaryAction = (item) => {
    props.secondaryAction(item);
  }

  const [form] = Form.useForm();

  function search(item) {
    const searchString = item.search;
    dispatch(talentsActions.filter({ searchString }));
  }

  return (
    <>
      {/* <div className='talents-list--header'>
        <Form onValuesChange={search} form={form} autoComplete="off">
          <Form.Item name='search' noStyle={true} className='input--grey'>
            <Input placeholder="Search" allowClear={true} bordered={false} className='input--grey' />
          </Form.Item>
        </Form>
      </div> */}
      <div className='talents-list--margin-top'>
        {
          talents.map((item) => {
            return (
              <TalentListItem
                key={'talent.' + item.id}
                selectable={selectable}
                selectedItems={selectedTalents}
                mainAction={mainAction}
                secondaryAction={secondaryAction}
                item={item}
                displayAs={props.displayAs}
              />
            );
          })
        }
      </div>
    </>
  );
}

export default TalentsList;