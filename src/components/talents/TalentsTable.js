import './TalentsTable.css';
import '../../helpers/shared.css';

import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { getTalents, getOrder, fetchTalents, talentsActions } from '../../store/talents/talents';
import TalentLocation from './components/TalentLocation';
import TalentUsername from './components/TalentUsername';
import TableHeaderCell from '../ui-components/TableHeaderCell';

function TalentsTable() {
    const dispatch = useDispatch();
    const talents = useSelector(getTalents);
    const [order, setOrder] = useState(useSelector(getOrder));

    useEffect(() => {
        dispatch(fetchTalents());
    }, [dispatch]);

    useEffect(() => {
        dispatch(talentsActions.sort({ order }));
    }, [order, dispatch]);

    const sortBy = (column) => {
        if (column === order.column) {
            setOrder({ 'column': column, 'asc': !order.asc });
        }
        else {
            setOrder({ 'column': column, 'asc': true });
        }
    };

    let result = null;

    if (talents
        && Object.keys(talents).length > 0) {

        result = talents.map((talent, index) => {
            return (
                <NavLink key={'talent.' + talent.id} to={talent.id}>
                    <div className='talents-table--row-container talents-table--row text-regular'>
                        <div><TalentUsername name={talent.name} /></div>
                        <div><TalentLocation location={talent.location} /></div>
                        <div>{talent.email}</div>
                        <div>{talent.phone}</div>
                    </div>
                </NavLink>
            );
        });
    }

    return (
        <div className='talents-table--table'>
            <div className='talents-table--row-container talents-table--header text-small text-light'>
                <TableHeaderCell text='Name' onClick={sortBy} sortColumn='name' order={order} />
                <TableHeaderCell text='Location' onClick={sortBy} sortColumn='location' order={order} />
                <TableHeaderCell text='Email' onClick={sortBy} sortColumn='email' order={order} />
                <TableHeaderCell text='Phone' onClick={sortBy} sortColumn='phone' order={order} />
            </div>
            {result}
        </div>
    );
}

export default TalentsTable;