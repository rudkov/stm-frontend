import './TalentsRoot.css';
import '../../helpers/shared.css';

function TalentsRoot() {
    return (
        <div className='talents-root--container text-regular text-light'>
            Please select a talent from the list.
        </div>
    );
}

export default TalentsRoot;