import './TalentUsername.css';
import '../../../helpers/shared.css';

import { Avatar } from 'antd';

function TalentUsername(props) {
    return (
        <div className="talent-username--container">
            <Avatar size="small" className="talent-username--avatar">{props.name?.charAt(0)}</Avatar>
            <span className="talent-username--name text-regular">{props.name}</span>
        </div>
    );
};
export default TalentUsername;