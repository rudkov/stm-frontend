import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Input, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

function TalentSectionFoodAllergies(props) {
    const talent = useSelector(getTalent);

    const { Option } = Select;
    const { TextArea } = Input;

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Food & Allergies</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        label='Allergies'
                        value={talent.allergies}
                    />
                    <TalentProfileCell
                        label='Vegetarian'
                        value={talent.is_vegetarian}
                    />
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        label='Allergies'
                        value={
                            <Form.Item name='allergies'>
                                <TextArea autoSize={{ minRows: 1 }} />
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Vegetarian'
                        value={
                            <Form.Item name='is_vegetarian'>
                                <Select allowClear>
                                    <Option value="Yes">Yes</Option>
                                    <Option value="No">No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                </div>
            </div>
        </div>
    );
}

export default TalentSectionFoodAllergies;