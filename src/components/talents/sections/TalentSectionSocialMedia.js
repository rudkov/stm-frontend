import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Input, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

import { ReactComponent as IconCrossInCircle } from '../../../assets/icons/cross-in-circle.svg';

import { ReactComponent as IconFacebook } from '../../../assets/icons/vendor/facebook.svg';
import { ReactComponent as IconInstagram } from '../../../assets/icons/vendor/instagram.svg';
import { ReactComponent as IconTiktok } from '../../../assets/icons/vendor/tiktok.svg';
import { ReactComponent as IconTwitter } from '../../../assets/icons/vendor/twitter.svg';
import { ReactComponent as IconWechat } from '../../../assets/icons/vendor/wechat.svg';
import { ReactComponent as IconYoutube } from '../../../assets/icons/vendor/youtube.svg';

import Button from '../../buttons/Button';

function TalentSectionSocialMedia(props) {
    const talent = useSelector(getTalent);
    const settings = useSelector((state) => state.settings.items);

    const { Option } = Select;
    const { TextArea } = Input;

    const icons = {
        'facebook': <IconFacebook />,
        'instagram': <IconInstagram />,
        'tiktok': <IconTiktok />,
        'twitter': <IconTwitter />,
        'wechat': <IconWechat />,
        'youtube': <IconYoutube />,
    };

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Social Media</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    {
                        talent.social_medias?.map((item, index) => {
                            return <TalentProfileCell
                                icon={icons[item.type?.system_name]}
                                key={`talent_social_media_.${item.id}`}
                                label={item.type?.name}
                                value={<a href={`${item.type?.url + item.info}`} target='_blank' rel='noreferrer'>{item.info}</a>}
                            />;
                        })
                    }
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>

                    <Form.List
                        name="social_medias"
                        initialValue={[
                            { social_media_type_id: null, info: '' }
                        ]}
                    >
                        {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, ...restField }) => (

                                    <TalentProfileCell
                                        key={`talent_social_media_.${key}`}
                                        value={
                                            <Input.Group compact>
                                                <Form.Item {...restField} name={[name, "social_media_type_id"]} style={{ width: '30%' }}>
                                                    <Select allowClear>
                                                        {
                                                            settings.social_media_types.map((item, index) => {
                                                                return <Option value={item.id} key={`social_media_type_.${item.id}`}><div className='select--badge select--badge-select'>{icons[item.system_name]}</div>{item.name}</Option>;
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item {...restField} name={[name, "info"]} style={{ width: '60%' }}>
                                                    <TextArea autoSize={{ minRows: 1 }} placeholder="Username" />
                                                </Form.Item>
                                                <Button key='talent.social_medias.remove' icon={IconCrossInCircle} isSmall={true} onClick={() => remove(name)} />
                                            </Input.Group>
                                        }
                                    />

                                ))}
                                <Form.Item>
                                    <Button key='talent.social_medias.add' title='Add social media' onClick={() => add()} />
                                </Form.Item>
                            </>
                        )}
                    </Form.List>

                </div>
            </div>
        </div>
    );
}

export default TalentSectionSocialMedia;