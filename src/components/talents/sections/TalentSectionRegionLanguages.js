import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

function TalentSectionRegionLanguages(props) {
    const talent = useSelector(getTalent);
    const settings = useSelector((state) => state.settings.items);

    const { Option } = Select;

    const citizenships = talent.citizenships?.map((elem) => { return elem.name }).join(", ");
    const languages = talent.languages?.map((elem) => { return elem.name }).join(", ");

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Region & Languages</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--2-columns text-regular'>
                    <TalentProfileCell
                        label='Languages'
                        value={languages}
                    />
                    <TalentProfileCell
                        label='Citizenships'
                        value={citizenships}
                    />
                    <TalentProfileCell
                        label='Accent'
                        value={talent.is_accent}
                    />
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--2-columns text-regular'>
                    <TalentProfileCell
                        label='Languages'
                        value={
                            <Form.Item name='languages'>
                                <Select
                                    mode="multiple"
                                    optionFilterProp="label"
                                    options={(settings.languages || []).map((d) => ({
                                        value: d.id,
                                        label: d.name,
                                    }))}
                                />
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Citizenships'
                        value={
                            <Form.Item name='citizenships'>
                                <Select
                                    mode="multiple"
                                    optionFilterProp="label"
                                    options={(settings.countries || []).map((d) => ({
                                        value: d.alpha_2,
                                        label: d.name,
                                    }))}
                                />
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Accent'
                        value={
                            <Form.Item name='is_accent'>
                                <Select allowClear>
                                    <Option value="Yes">Yes</Option>
                                    <Option value="No">No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                </div>
            </div>
        </div>
    );
}

export default TalentSectionRegionLanguages;