import '../TalentProfile.css';
import '../../../helpers/shared.css';

import { useSelector } from 'react-redux';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

function TalentSectionSystemInfo() {
    const talent = useSelector(getTalent);

    return (
        <div className='talent-profile--section talent-profile--system-info'>
            <div className='talent-profile--section--2-columns text-regular'>
                <TalentProfileCell
                    label='Updated'
                    value={talent.updated_by?.name + ' · ' + talent.updated_at}
                />
                <TalentProfileCell
                    label='Created'
                    value={talent.created_by?.name + ' · ' + talent.created_at}
                />
            </div>
        </div>
    );
}

export default TalentSectionSystemInfo;