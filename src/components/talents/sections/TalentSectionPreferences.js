import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

import { ReactComponent as IconYes } from '../../../assets/icons/yes.svg';
import { ReactComponent as IconNo } from '../../../assets/icons/no.svg';

function TalentSectionPreferences(props) {
    const talent = useSelector(getTalent);

    const { Option } = Select;

    const icons = {
        'Yes': <IconYes />,
        'No': <IconNo />,
    };

    return (
        <div className='talent-profile--section'>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Preferences</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--4-columns text-regular'>
                    <TalentProfileCell
                        icon={icons[talent.is_lingerie]}
                        noLabel={true}
                        value='Lingerie'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_nude]}
                        noLabel={true}
                        value='Nude'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_fur]}
                        noLabel={true}
                        value='Fur'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_liquor_ads]}
                        noLabel={true}
                        value='Liquor ads'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_smoking_ads]}
                        noLabel={true}
                        value='Smoking ads'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_gambling_ads]}
                        noLabel={true}
                        value='Gambling ads'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_faithbased_ads]}
                        noLabel={true}
                        value='Faithbased ads'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_political_ads]}
                        noLabel={true}
                        value='Political ads'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_topless]}
                        noLabel={true}
                        value='Topless'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_swimwear]}
                        noLabel={true}
                        value='Swimwear'
                    />
                    <TalentProfileCell
                        icon={icons[talent.is_sports]}
                        noLabel={true}
                        value='Sports'
                    />
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--4-columns text-regular'>
                    <TalentProfileCell
                        label='Lingerie'
                        value={
                            <Form.Item name='is_lingerie'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Nude'
                        value={
                            <Form.Item name='is_nude'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Fur'
                        value={
                            <Form.Item name='is_fur'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Liquor ads'
                        value={
                            <Form.Item name='is_liquor_ads'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Smoking ads'
                        value={
                            <Form.Item name='is_smoking_ads'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Gambling ads'
                        value={
                            <Form.Item name='is_gambling_ads'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Faithbased ads'
                        value={
                            <Form.Item name='is_faithbased_ads'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Political ads'
                        value={
                            <Form.Item name='is_political_ads'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Topless'
                        value={
                            <Form.Item name='is_topless'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Swimwear'
                        value={
                            <Form.Item name='is_swimwear'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                    <TalentProfileCell
                        label='Sports'
                        value={
                            <Form.Item name='is_sports'>
                                <Select allowClear>
                                    <Option value="Yes"><div className='select--badge select--badge-select'>{icons['Yes']}</div>Yes</Option>
                                    <Option value="No"><div className='select--badge select--badge-select'>{icons['No']}</div>No</Option>
                                </Select>
                            </Form.Item>
                        }
                    />
                </div>
            </div>
        </div>
    );
}

export default TalentSectionPreferences;