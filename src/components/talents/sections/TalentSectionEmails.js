import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Input, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

import { ReactComponent as IconCrossInCircle } from '../../../assets/icons/cross-in-circle.svg';

import Button from '../../buttons/Button';

function TalentSectionEmails(props) {
    const talent = useSelector(getTalent);
    const settings = useSelector((state) => state.settings.items);

    const { Option } = Select;
    const { TextArea } = Input;

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Emails</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    {
                        talent.emails?.map((item, index) => {
                            return <TalentProfileCell
                                key={`talent_email_.${item.id}`}
                                label={item.type?.name}
                                value={<a href={`mailto:${item.info}`}>{item.info}</a>}
                            />;
                        })
                    }
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>

                    <Form.List
                        name="emails"
                        initialValue={[
                            { email_type_id: null, info: '' }
                        ]}
                    >
                        {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, ...restField }) => (

                                    <TalentProfileCell
                                        key={`talent_email_.${key}`}
                                        value={
                                            <Input.Group compact>
                                                <Form.Item {...restField} name={[name, "email_type_id"]} style={{ width: '30%' }}>
                                                    <Select allowClear>
                                                        {
                                                            settings.email_types.map((item, index) => {
                                                                return <Option value={item.id} key={`email_type_.${item.id}`}>{item.name}</Option>;
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item {...restField} name={[name, "info"]} style={{ width: '60%' }}>
                                                    <TextArea autoSize={{ minRows: 1 }} placeholder="Email" />
                                                </Form.Item>
                                                <Button key='talent.emails.remove' icon={IconCrossInCircle} isSmall={true} onClick={() => remove(name)} />
                                            </Input.Group>
                                        }
                                    />


                                ))}
                                <Form.Item>
                                    <Button key='talent.emails.add' title='Add email' onClick={() => add()} />
                                </Form.Item>
                            </>
                        )}
                    </Form.List>

                </div>
            </div>
        </div>
    );
}

export default TalentSectionEmails;