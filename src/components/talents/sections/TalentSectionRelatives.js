import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Input, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

import { ReactComponent as IconCrossInCircle } from '../../../assets/icons/cross-in-circle.svg';

import Button from '../../buttons/Button';

function TalentSectionRelatives(props) {
    const talent = useSelector(getTalent);
    const settings = useSelector((state) => state.settings.items);

    const { Option } = Select;
    const { TextArea } = Input;

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Relatives</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    {
                        talent.relatives?.map((relative, index) => {
                            return <TalentProfileCell
                                key={`talent_relative_.${relative.id}`}
                                label={relative.type?.name}
                                value={relative.info}
                            />;
                        })
                    }
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>

                    <Form.List
                        name="relatives"
                        initialValue={[
                            { relative_type_id: null, info: '' }
                        ]}
                    >
                        {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, ...restField }) => (

                                    <TalentProfileCell
                                        key={`talent_relative_.${key}`}
                                        value={
                                            <Input.Group compact>
                                                <Form.Item {...restField} name={[name, "relative_type_id"]} style={{ width: '30%' }}>
                                                    <Select allowClear>
                                                        {
                                                            settings.talent_relative_types.map((item, index) => {
                                                                return <Option value={item.id} key={`talent_relative_type_.${item.id}`}>{item.name}</Option>;
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item {...restField} name={[name, "info"]} style={{ width: '60%' }}>
                                                    <TextArea autoSize={{ minRows: 1 }} placeholder="Relative info" />
                                                </Form.Item>
                                                <Button key='talent.relatives.remove' icon={IconCrossInCircle} isSmall={true} onClick={() => remove(name)} />
                                            </Input.Group>
                                        }
                                    />


                                ))}
                                <Form.Item>
                                    <Button key='talent.relatives.add' title='Add relative' onClick={() => add()} />
                                </Form.Item>
                            </>
                        )}
                    </Form.List>





                </div>
            </div>
        </div>
    );
}

export default TalentSectionRelatives;