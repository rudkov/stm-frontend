import '../TalentProfile.css';
import '../../../helpers/shared.css';

import { useSelector } from 'react-redux';

import { Form, Input } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

function TalentSectionNotes(props) {
    const talent = useSelector(getTalent);

    const { TextArea } = Input;

    return (
        <div className='talent-profile--section'>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Notes</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        className='invisible-bottom-border'
                        value={talent.comment}
                    />
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        value={
                            <Form.Item name='comment' className='form-item--border-bottom'>
                                <TextArea autoSize={{ minRows: 1 }} />
                            </Form.Item>
                        }
                    />
                </div>
            </div>
        </div>
    );
}

export default TalentSectionNotes;