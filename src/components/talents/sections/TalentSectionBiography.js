import '../TalentProfile.css';
import '../../../helpers/shared.css';

import { useSelector } from 'react-redux';

import { Form, Input } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

function TalentSectionBiography(props) {
    const talent = useSelector(getTalent);

    const { TextArea } = Input;

    return (
        <div className='talent-profile--section'>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Biography</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        value={talent.biography}
                    />
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    <TalentProfileCell
                        value={
                            <Form.Item name='biography'>
                                <TextArea autoSize={{ minRows: 1 }} />
                            </Form.Item>
                        }
                    />
                </div>
            </div>
        </div>
    );
}

export default TalentSectionBiography;