import '../TalentProfile.css';
import '../../../helpers/shared.css';
import '../../../helpers/form.css';

import { useSelector } from 'react-redux';

import { Form, Input, Select } from 'antd';

import { getTalent } from '../../../store/talents/talent';
import TalentProfileCell from '../TalentProfileCell';

import { ReactComponent as IconCrossInCircle } from '../../../assets/icons/cross-in-circle.svg';

import { ReactComponent as IconFacebookMessenger } from '../../../assets/icons/vendor/facebook-messenger.svg';
import { ReactComponent as IconTelegram } from '../../../assets/icons/vendor/telegram.svg';
import { ReactComponent as IconWhatsapp } from '../../../assets/icons/vendor/whatsapp.svg';

import Button from '../../buttons/Button';

function TalentSectionMessengers(props) {
    const talent = useSelector(getTalent);
    const settings = useSelector((state) => state.settings.items);

    const { Option } = Select;
    const { TextArea } = Input;

    const icons = {
        'facebook-messenger': <IconFacebookMessenger />,
        'telegram': <IconTelegram />,
        'whatsapp': <IconWhatsapp />,
    };

    return (
        <div>
            <div className='talent-profile--section--header'>
                <div className='talent-profile--section--title text-regular'>Messengers</div>
            </div>
            <div className={`${!props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>
                    {
                        talent.messengers?.map((item, index) => {
                            return <TalentProfileCell
                                icon={icons[item.type?.system_name]}
                                key={`talent_messenger_.${item.id}`}
                                label={item.type?.name}
                                value={<a href={`${item.type?.url + item.info}`} target='_blank' rel='noreferrer'>{item.info}</a>}
                            />;
                        })
                    }
                </div>
            </div>
            <div className={`${props.editMode ? "" : "hidden"}`}>
                <div className='talent-profile--section--1-column text-regular'>

                    <Form.List
                        name="messengers"
                        initialValue={[
                            { messengers_type_id: null, info: '' }
                        ]}
                    >
                        {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, ...restField }) => (

                                    <TalentProfileCell
                                        key={`talent_messenger_.${key}`}
                                        value={
                                            <Input.Group compact>
                                                <Form.Item {...restField} name={[name, "messenger_type_id"]} style={{ width: '30%' }}>
                                                    <Select allowClear>
                                                        {
                                                            settings.messenger_types.map((item, index) => {
                                                                return <Option value={item.id} key={`messenger_type_.${item.id}`}><div className='select--badge select--badge-select'>{icons[item.system_name]}</div>{item.name}</Option>;
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item {...restField} name={[name, "info"]} style={{ width: '60%' }}>
                                                    <TextArea autoSize={{ minRows: 1 }} placeholder="Username" />
                                                </Form.Item>
                                                <Button key='talent.messengers.remove' icon={IconCrossInCircle} isSmall={true} onClick={() => remove(name)} />
                                            </Input.Group>
                                        }
                                    />

                                ))}
                                <Form.Item>
                                    <Button key='talent.messengers.add' title='Add messenger' onClick={() => add()} />
                                </Form.Item>
                            </>
                        )}
                    </Form.List>

                </div>
            </div>
        </div>
    );
}

export default TalentSectionMessengers;