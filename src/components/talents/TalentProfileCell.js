import './TalentProfile.css';
import '../../helpers/shared.css';

function TalentProfileCell(props) {
    let label = '';
    let info = '';
    let icon = '';
    let value = '';

    if (props.label) {
        label = <div className='talent-profile--label'>{props.label}</div>;
    }

    if (props.info) {
        info = <span className='text-light'><span className='text-separator'>·</span>{props.info}</span>;
    }

    if (props.icon) {
        icon = <span className='talent-profile--value-icon'>{props.icon}</span>;
    }

    if (props.value) {
        value =
            <div className='talent-profile--value-container'>
                {icon}<div className='talent-profile--value'>{props.value}{info}</div>
            </div>
            ;
    }

    if (props.noLabel && !props.icon) {
        value =
            <div className='talent-profile--value-container'>
                {icon}<div className='talent-profile--value talent-profile--value-placeholder'>{props.value}{info}</div>
            </div>
            ;
    }

    return (
        <div className={props?.className}>
            {label}
            {value}
        </div>
    );
}

export default TalentProfileCell;