import './TalentsHeader.css';
import '../../helpers/form.css';

import { Button, Form, Input, Space } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { useEffect, useState } from 'react';

import { talentsActions, getQuery } from '../../store/talents/talents';

import { ReactComponent as IconSearch } from '../../assets/icons/search.svg';
import { ReactComponent as IconInTown } from '../../assets/icons/in-town.svg';
import { ReactComponent as IconFilter } from '../../assets/icons/filter.svg';

function TalentsHeader() {
    const dispatch = useDispatch();
    const [form] = Form.useForm();

    const [query, setQuery] = useState(useSelector(getQuery));

    const search = (item) => {
        setQuery({ 'searchString': item.search, 'inTownOnly': query.inTownOnly });
    }

    const toggleInTown = () => {
        setQuery({ 'searchString': query.searchString, 'inTownOnly': !query.inTownOnly });
    }

    useEffect(() => {
        dispatch(talentsActions.filter({ query }));
    }, [query, dispatch]);

    return (
        <div className='talents-header--container'>
            <div className='talents-header--left'>
                <Space wrap>
                    <Form onValuesChange={search} form={form} autoComplete="off" initialValues={{search:query.searchString}}>
                        <Form.Item name='search' noStyle={true}>
                            <Input placeholder="Search" allowClear={true} prefix={<IconSearch />} className='input--contained' />
                        </Form.Item>
                    </Form>
                    <Button icon={<IconInTown />} onClick={toggleInTown} className={`button--icon ${query.inTownOnly ? 'button--icon--active' : ''}`} />
                    <Button icon={<IconFilter />} className={`button--icon`} disabled />
                </Space>
            </div>
            <div className='talents-header--right'>
                <NavLink to='new'>
                    <Button type="primary" className={`button--primary`}>Create Talent</Button>
                </NavLink>
            </div>
        </div >
    );
}

export default TalentsHeader;