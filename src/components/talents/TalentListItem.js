import './TalentListItem.css';

import { Avatar } from 'antd';

import { NavLink } from 'react-router-dom';

import Button from '../buttons/Button';
import { ReactComponent as IconCrossInCircle } from '../../assets/icons/cross-in-circle.svg';

function TalentListItem(props) {
    const name = props.item.name || [props.item.first_name, props.item.last_name].join(' ');

    let isSelected = false;
    let removeButton = () => { };

    const mainAction = (item) => {
        props.mainAction(item);
    }

    if (props.selectable === true) {
        isSelected = props.selectedItems.includes(props.item.id);
    }

    if (props.canBeRemoved === true) {
        const removeAction = (item) => {
            props.removeAction(item);
        }
        removeButton = () => {
            return (
                <div className='talents-list-item--controls'>
                    <Button icon={IconCrossInCircle} isSmall={true} onClick={removeAction.bind(this, props.item)} />
                </div>
            );
        }
    }

    if (props.displayAs === 'link') {
        return (
            <div className="talents-list-item">
                <NavLink className={({ isActive }) => "talents-list-item--item" + (isActive ? " active" : "")} to={props.item.id}>
                    <Avatar size="small" className="talents-list-item--avatar">{name?.charAt(0)}</Avatar><span className="talents-list-item--name">{name}</span>
                </NavLink>
            </div>
        );
    }
    else {
        return (
            <div className="talents-list-item">
                <div className={`talents-list-item--item ${isSelected ? "talents-list-item--item-selected" : ""}`} onClick={mainAction.bind(this, props.item)}>
                    <Avatar size="small" className="talents-list-item--avatar">{name?.charAt(0)}</Avatar><span className="talents-list-item--name">{name}</span>
                    {removeButton()}
                </div>
            </div>
        );
    }
}

export default TalentListItem;