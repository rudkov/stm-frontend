import './Main.css';

import { Outlet } from 'react-router-dom';

import Menu from './Menu';

const Main = () => {
    return (
        <div className='main--container'>
            <div className='main--sidebar'><Menu /></div>
            <div className='main--main'>
                <Outlet />
            </div>
        </div>
    );
}

export default Main;