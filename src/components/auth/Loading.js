import './Loading.css';

const Loading = () => {
    return (
        <div className='loading--container'>
            <div className="loading--loader">Loading...</div>
        </div>
    );
}

export default Loading;