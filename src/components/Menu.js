import './Menu.css';

import { NavLink } from 'react-router-dom';

import Button from './buttons/Button';

import { ReactComponent as IconTalents } from '../assets/icons/talents.svg';
import { ReactComponent as IconCalendar } from '../assets/icons/calendar.svg';
import { ReactComponent as IconContacts } from '../assets/icons/contacts.svg';
import { ReactComponent as IconLogout } from '../assets/icons/logout.svg';

function Menu() {
    return (
        <div className='menu'>
            <NavLink to='talents'>
                <Button key='menu.talents' icon={IconTalents} tooltip='Talents' />
            </NavLink>
            <NavLink to='calendar'>
                <Button key='menu.calendar' icon={IconCalendar} tooltip='Calendar' />
            </NavLink>
            <NavLink to='packages'>
                <Button key='menu.address_book' icon={IconContacts} tooltip='Address book' />
            </NavLink>
            <NavLink to='logout'>
                <Button key='menu.logout' icon={IconLogout} tooltip='Logout' />
            </NavLink>
        </div>
    );
}

export default Menu;