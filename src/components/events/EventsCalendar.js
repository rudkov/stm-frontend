import './EventsCalendar.css';

import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

import axios from 'axios';

import * as dayjs from 'dayjs';
import en from "dayjs/locale/en";
import * as weekday from 'dayjs/plugin/weekday';
import _ from 'lodash';

import DayItem from './DayItem';
import { eventsActions } from '../../store/events/events';

function EventsCalendar() {

    const dispatch = useDispatch();

    let shouldBeRefreshed = useSelector((state) => state.events.shouldBeRefreshed) || [];
    let events = useSelector((state) => state.events.items) || [];
    let datesToShow = useSelector((state) => state.events.dates) || [];
    let talents = useSelector((state) => state.events.talents) || [];

    dayjs.locale({
        ...en,
        weekStart: 1
    });

    dayjs.extend(weekday);

    // const params = useParams();

    let firstDayOfCurrentMonth;

    // console.log(datesToShow);

    if (!datesToShow.year || !datesToShow.month || !datesToShow.day) {
        firstDayOfCurrentMonth = dayjs().set('date', 1);
    }
    else {
        firstDayOfCurrentMonth = dayjs(datesToShow.year + '/' + datesToShow.month + '/' + datesToShow.day);
    }

    // console.log(firstDayOfCurrentMonth);

    const WEEKDAYS = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

    const daysInMonth = firstDayOfCurrentMonth.daysInMonth();
    const daysInPreviousMonth = firstDayOfCurrentMonth.subtract(dayjs().get('D'), 'day').daysInMonth();
    const firstDayInMonth = firstDayOfCurrentMonth.date(1).weekday();
    const firstDayInPreviousMonth = daysInPreviousMonth - firstDayInMonth + 1;

    const previousMonth = firstDayOfCurrentMonth.subtract(dayjs().get('D'), 'day');
    // const nextMonth = dayjs().subtract(dayjs().get('D'), 'day');

    const firstDayOfCal = previousMonth.set('date', firstDayInPreviousMonth);

    let dates = [];

    let numberOfDays = 0;

    numberOfDays = daysInPreviousMonth - firstDayInPreviousMonth + 1 + daysInMonth;
    numberOfDays = numberOfDays + 7 - numberOfDays % 7;

    for (let i = 0; i < numberOfDays; i++) {
        dates = [...dates, firstDayOfCal.add(i, 'day')];
    }

    const weeks = _.chunk(dates, 7);

    if (shouldBeRefreshed === true) {
        axios({
            method: 'get',
            url: '/api/v1/events',
            params: {
                year: firstDayOfCurrentMonth.get('y'),
                month: firstDayOfCurrentMonth.get('M') + 1,
                day: firstDayOfCurrentMonth.get('D'),
                talents,
            }
        })
            .then(function (response) {
                let items = response.data;
                dispatch(eventsActions.updateListFromServer({ items }));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <div className='events-calendar'>
            <table className='calendar'>
                <thead>
                    <tr>
                        {WEEKDAYS.map((day, index) => {
                            return (
                                <td className='calendar-day calendar-day--weekday' key={'head.' + index}>{day}</td>
                            )
                        })}
                    </tr>
                </thead>
                <tbody>
                    {weeks.map((week, index) => {
                        return (
                            <tr key={'week.' + index}>
                                {week.map((day, sIndex) => {
                                    return <DayItem
                                        data={events}
                                        day={day}
                                        isSelectedMonth={(firstDayOfCurrentMonth.get('M') === day.get('M')) ? true : false}
                                        key={'day.' + day.get('D') + '.' + day.get('M')}
                                    />
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default EventsCalendar;