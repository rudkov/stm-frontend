import './EventInfo.css';
import '../../helpers/event-type.css';
import '../../helpers/form.css';

import React from 'react';
import axios from 'axios';

import { Form, Input, Select, DatePicker } from 'antd';

import { useSelector, useDispatch } from 'react-redux';
import { eventActions } from '../../store/events/event';

import TalentListItem from '../talents/TalentListItem';
import TimePicker from './TimePicker';
import dayjs from 'dayjs';

import Button from '../buttons/Button';
import { ReactComponent as IconClose } from '../../assets/icons/close.svg';

function initForm(form, item) {
  form.setFieldsValue({
    title: item.title,
    event_type_id: item.event_type_id,
  });
  return form;
}

function EventInfo() {

  const { Option } = Select;
  const { TextArea } = Input;

  const dispatch = useDispatch();

  let settings = useSelector((state) => state.settings.items);

  let shouldBeRefreshed = useSelector((state) => state.event.shouldBeRefreshed);
  let selectedEvent = useSelector((state) => state.event.selectedItem);
  let event = useSelector((state) => state.event.item) || {};

  const [form] = Form.useForm();

  let item;

  if (shouldBeRefreshed === true) {
    axios({
      method: 'get',
      url: '/api/v1/events/' + selectedEvent.id,
    })
      .then(function (response) {
        item = response.data;
        initForm(form, item);
        dispatch(eventActions.update({ item }));
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const closePanel = () => {
    const item = {};
    dispatch(eventActions.setSelectedItem({ item }));
    initForm(form, item);
    dispatch(eventActions.update({ item }));
  }

  const removeTalentFromEvent = () => {
    console.log('remove me');
}

  const saveEvent = () => {
  }

  return (
    <div className='event-info--container'>
      <Form
        name="event"
        form={form}
      >
        <div className='event-info--top-bar'>
          <div className='event-info--controls-group'>
            <Form.Item name='event_type_id' noStyle={true} key='eventInfo.eventType'>
              <Select style={{ width: 120 }} className='input--background-on-hover' bordered={false} showArrow={false}>
                {
                  settings.event_types.map((item, index) => {
                    return <Option value={item.id} key={`eventInfo.eventTypes.${item.id}`}><div className={`event-info--type-badge event-type--${item.system_name}`}></div> {item.name}</Option>;
                  })
                }
              </Select>
            </Form.Item>
          </div>
          <div className='event-info--controls-group event-info--controls-group-right event-info--controls-group-with-gap'>
            {/* <Button key='eventInfo.saveEvent' title='Save' type='primary' onClick={saveEvent} /> */}
            <Button key='eventInfo.closePanel' icon={IconClose} onClick={closePanel} />
          </div>
        </div>

        <Form.Item name='title' key='eventInfo.title'>
          <TextArea autoSize={true} size="large" placeholder="Event title" bordered={false} className='input--background-on-hover event-info--input-title' />
        </Form.Item>

        <div>
          <div className='event-info--section-header'>Talents</div>
          <div className='event-info--section-content'>
            {
              event.talents &&
              event.talents.map((item, index) => {
                return (
                  <TalentListItem
                    key={'eventInfo.talent.' + item.id}
                    canBeRemoved={true}
                    mainAction={()=>{}}
                    removeAction={removeTalentFromEvent}
                    item={item}
                  />
                );
              })
            }
          </div>
        </div>

        <div>
          <div className='event-info--section-header'>Time</div>
          <div className='event-info--section-content'>
            {
              event.event_chunks &&
              event.event_chunks.map((item, index) => {
                return (
                  <div key={'event.dates.' + item.id}>
                    <DatePicker
                      bordered={false}
                      suffixIcon={false}
                      clearIcon={false}
                      format={'D MMMM YYYY'}
                      className='input--background-on-hover'
                      key={'event.start_date.' + item.id}
                      // style={{width: '140px'}}
                      defaultValue={dayjs(item.start_date)}
                    />
                    <TimePicker
                      bordered={false}
                      showArrow={false}
                      key={'event.start_time.' + item.id}
                      date={item.start_date}
                    />–
                    <TimePicker
                      bordered={false}
                      showArrow={false}
                      key={'event.end_time.' + item.id}
                      date={item.end_date}
                    />
                  </div>
                );
              })
            }
          </div>
        </div>

      </Form>
    </div >
  );
}

export default EventInfo;