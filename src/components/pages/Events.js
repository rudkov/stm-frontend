import './Events.css';

import EventsHeader from '../events/EventsHeader';
import EventsCalendar from '../events/EventsCalendar';
import TalentsList from '../talents/TalentsList';
import EventInfo from '../events/EventInfo';

import { eventsActions } from '../../store/events/events';

import { useDispatch, useSelector } from 'react-redux';

function Events() {
    const dispatch = useDispatch();
    let selectedEvent = useSelector((state) => state.event.selectedItem);

    let hiddenPanelClassName = '';
    if (selectedEvent
        && Object.keys(selectedEvent).length === 0
        && Object.getPrototypeOf(selectedEvent) === Object.prototype) {
        hiddenPanelClassName = ' page-events--sidebar-hidden';
    }

    const talentsListItemClickHandler = (item) => {
        dispatch(eventsActions.addOrRemoveTalent({ item }));
        dispatch(eventsActions.updateList());
    }

    return (
        <div className={`page-events--container ${hiddenPanelClassName}`}>
            <div className='page-events--sidebar'>
                <TalentsList mainAction={talentsListItemClickHandler} selectable={true} />
            </div>
            <div className='page-events--main'>
                <div className='page-events--header'>
                    <EventsHeader />
                </div>
                <div className='page-events--margin-top'>
                    <EventsCalendar />
                </div>
            </div>
            <div className={`page-events--sidebar page-events--margin-top ${hiddenPanelClassName === '' ? '' : 'hidden'}`}>
                <EventInfo />
            </div>
        </div>
    );
}

export default Events;