import './Talents.css';

import { useEffect, useState, useCallback } from 'react';

import TalentsTable from '../talents/TalentsTable';
import { Outlet } from 'react-router-dom';
import TalentsHeader from '../talents/TalentsHeader';

function Talents() {
    const minWidth = 500;
    const interfaceMinWidth = 300;
    const resizeHandlePadding = 6;

    const [startDragging, setStartDragging] = useState(false);

    const mouseDown = () => {
        setStartDragging(true);
        document.getElementsByTagName('body')[0].style.userSelect = 'none';
    };

    const mouseMove = useCallback((e) => {
        const currentWidth = document.getElementById('page-container').lastChild.offsetWidth;
        const maxWidth = window.innerWidth - interfaceMinWidth;
        const mouseX = e.clientX;
        const offset = document.getElementById('page-container').lastChild.offsetLeft;

        const width = currentWidth + offset - mouseX - resizeHandlePadding;

        if (width >= minWidth && width <= maxWidth) {
            document.getElementById('page-container').style.gridTemplateColumns = `auto 11px ${width}px`;
        }
    }, [minWidth]);

    const removeListeners = useCallback(() => {
        window.removeEventListener("mousemove", mouseMove);
        window.removeEventListener("mouseup", removeListeners);
    }, [mouseMove]);

    const mouseUp = useCallback(() => {
        setStartDragging(false);
        document.getElementsByTagName('body')[0].style.userSelect = 'auto';
        removeListeners();
    }, [setStartDragging, removeListeners]);

    useEffect(() => {
        if (startDragging) {
            window.addEventListener("mousemove", mouseMove);
            window.addEventListener("mouseup", mouseUp);
        }

        return () => {
            removeListeners();
        };

    }, [startDragging, mouseMove, mouseUp, removeListeners]);

    return (
        <div id='page-container' className='page-talents--container'>
            <div className='page-talents--list'>
                <TalentsHeader />
                <TalentsTable />
            </div>
            <div className='page-talents--resize-handle' onMouseDown={mouseDown}>
                <div></div>
            </div>
            <div className='page-talents--profile'>
                <Outlet />
            </div>
        </div>
    );
}

export default Talents;